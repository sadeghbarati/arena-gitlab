import { breakpointsTailwind } from '@vueuse/core'

// "sm": 640,
// "md": 768,
// "lg": 1024,
// "xl": 1280,
// "2xl": 1536
export const breakpoints = useBreakpoints(breakpointsTailwind)
export const isBiggerThanMd = breakpoints.greaterOrEqual('md')

export const isChooseAuthModalOpen = ref(false)

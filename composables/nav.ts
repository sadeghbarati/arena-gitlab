export const useMenuExpanded = () => useState<boolean>('menu-expanded', () => false)

export const useNav = (isMenuExpanded = useMenuExpanded()) => {
  watch(isBiggerThanMd,
    (isBigger) => {
      if (isBigger) { isMenuExpanded.value = false }
    })

  return {
    isMenuExpanded
  }
}

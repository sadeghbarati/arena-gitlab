import { ErrorMessage, Field, Form } from 'vee-validate'
// configure

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('Form', Form)
  nuxtApp.vueApp.component('Field', Field)
  nuxtApp.vueApp.component('ErrorMessage', ErrorMessage)
})

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    Form: typeof Form
    Field: typeof Field
    ErrorMessage: typeof ErrorMessage
  }
}

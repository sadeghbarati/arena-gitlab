// eslint-disable-next-line @typescript-eslint/no-var-requires
const { defineConfig } = require('eslint-define-config')

module.exports = defineConfig({
  extends: '@nuxtjs/eslint-config-typescript',
  rules: {
    'import/first': 'off',
    'no-console': 'off',
    'vue/multi-word-component-names': 'off'
  },
  ignorePatterns: [
    'dist',
    'node_modules',
    '.output',
    '.nuxt',
    '*.json'
  ]
})

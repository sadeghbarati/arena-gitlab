/* eslint-disable @typescript-eslint/no-unused-vars */
import { createResolver } from '@nuxt/kit'

const isDev = process.env.NODE_ENV === 'development'
const { resolve } = createResolver(import.meta.url)

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  extends: [
    './ui'
  ],
  css: [
    '@unocss/reset/tailwind.css',
    'swiper/scss'
  ],
  modules: [
    // '@nuxtjs/i18n',
    '@nuxt/content',
    '@vueuse/nuxt',
    '@nuxt/image-edge'
    // '@sidebase/nuxt-auth',
    // '@unlighthouse/nuxt'
  ],
  experimental: {
    inlineSSRStyles: false
  },
  imports: {
    dirs: [
      '~/stores'
    ],
    presets: [
      'vee-validate',
      {
        from: 'yup',
        imports: [
          ['ref', 'yupRef']
        ]
      }
    ]
    // dirs: ['stores'],
    // addons: {
    //   vueTemplate: true,
    // },
    // presets: [
    //   {
    //     from: '@headlessui/vue',
    //     imports: [
    //       'Tab',
    //       'TabGroup',
    //       'TabList',
    //       'TabPanel',
    //       'TabPanels',
    //     ],
    //   },
    // ],
  },
  typescript: {
    shim: false,
    strict: false
  },
  components: [
    {
      path: '~/components',
      global: true,
      pathPrefix: false
    }
  ],
  build: {
    transpile: isDev ? [] : ['@headlessui/vue']
  },
  postcss: {
    plugins: {
      'postcss-nested': {}
    }
  },
  app: {
    pageTransition: false,
    layoutTransition: false,
    head: {
      charset: 'utf-8',
      title: 'Arena Website',
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1.0, viewport-fit=cover'
        }
      ]
    }
  },
  content: {
    defaultLocale: 'en',
    locales: ['en', 'ar']
  },
  // image: {
  //   domains: []
  // },
  // auth: {
  //   enableSessionRefreshOnWindowFocus: true
  //   origin:  https://your-origin.com
  // },
  unocss: {
    components: false
  }
  // vueuse: {
  //   ssrHandlers: true,
  // },
  // i18n: {
  //   lazy: true,
  //   strategy: 'prefix_except_default',
  //   defaultLocale: 'en',
  //   vueI18n: {
  //     legacy: false,
  //   },
  //   locales: [
  //     {
  //       name: 'English',
  //       dir: 'ltr',
  //       code: 'en',
  //       iso: 'en-US',
  //       file: 'en-US.yml',
  //     },
  //     {
  //       name: 'Arabic',
  //       dir: 'rtl',
  //       code: 'ar',
  //       iso: 'ar-AE',
  //       file: 'ar-AE.yml',
  //     },
  //   ],
  //   detectBrowserLanguage: {
  //     useCookie: true,
  //     cookieKey: 'i18n_redirected',
  //     redirectOn: 'root',
  //   },
  // },
})

export default defineNuxtConfig({
  modules: [
    '@unocss/nuxt'
  ],
  components: [
    { path: './components', prefix: 'A', global: true }
  ]
})
